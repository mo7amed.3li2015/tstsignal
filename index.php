<?php include 'include/header.php'; ?>

<!--slider section start-->
    <div class="slider-container overlay">
        <!-- Slider Image -->
        <div id="mainSlider" class="nivoSlider slider-image">
            <img src="img/slider/slider-img1.jpg" alt="" title="#htmlcaption1"/>
            <img src="img/slider/slider-img2.jpg" alt="" title="#htmlcaption2"/>
            <img src="img/slider/slider-img3.jpg" alt="" title="#htmlcaption3"/>
        </div>
        <!-- Slider Caption 1 -->
        <div id="htmlcaption1" class="nivo-html-caption slider-caption-1">
            <div class="slider-text-table">
                <div class="slider-text-tablecell">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="hidden-xs social-media-follow">
                                <div class="social-box-inner">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-dribbble"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    </ul>	
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="slide-text">
                                    <div class="middle-text">
                                        <div class="title-1 wow slideInUp" data-wow-duration="0.9s" data-wow-delay="0s">
                                            <h2>welcome to Tstsignal</h2>
                                        </div>	
                                        <div class="desc wow slideInUp" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod rgtre<br> tempor incididunt ut labore et dolore magna aliquaaperiam, eaque ipsa quae<br> abillo inventore veritatis et magnam </p>
                                        </div>
                                        <div class="learn-more wow slideInUp" data-wow-duration="1.3s" data-wow-delay=".3s">
                                            <a href="about-us.php">Learn more</a>
                                        </div>	
                                    </div>	
                                </div>	
                                <div class="slide-caption-img slideInUp animated" data-wow-delay="1.2s" data-wow-duration="0.5s">
                                    <img src="img/slider/caption.png" alt="">
                                </div>			
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Caption 2 -->
        <div id="htmlcaption2" class="nivo-html-caption slider-caption-2">
            <div class="slider-text-table">
                <div class="slider-text-tablecell">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="hidden-xs social-media-follow">
                                <div class="social-box-inner">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-dribbble"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    </ul>	
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="slide-text">
                                    <div class="middle-text">
                                        <div class="title-1 wow slideInUp" data-wow-duration="1.2s" data-wow-delay="0.0s">
                                            <h2>welcome to Tstsignal</h2>
                                        </div>	
                                        <div class="desc wow slideInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod rgtre<br> tempor incididunt ut labore et dolore magna aliquaaperiam, eaque ipsa quae<br> abillo inventore veritatis et magnam </p>
                                        </div>
                                        <div class="learn-more wow slideInUp" data-wow-duration="1.3s" data-wow-delay=".3s">
                                            <a href="about-us.php">Learn more</a>
                                        </div>	
                                    </div>	
                                </div>	
                                <div class="slide-caption-img slideInUp  animated" data-wow-delay="1.2s" data-wow-duration="0.5s">
                                    <img src="img/slider/caption.png" alt="">
                                </div>			
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Caption 3 -->
        <div id="htmlcaption3" class="nivo-html-caption slider-caption-3">
            <div class="slider-text-table">
                <div class="slider-text-tablecell">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="hidden-xs social-media-follow">
                                <div class="social-box-inner">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-dribbble"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-pinterest"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    </ul>	
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-1 col-sm-12 col-xs-12">
                                <div class="slide-text">
                                    <div class="middle-text">
                                        <div class="title-1 wow slideInUp" data-wow-duration="0.9s" data-wow-delay="0s">
                                            <h2>welcome to Tstsignal</h2>
                                        </div>	
                                        <div class="desc wow slideInUp" data-wow-duration="1.2s" data-wow-delay="0.2s">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod rgtre<br> tempor incididunt ut labore et dolore magna aliquaaperiam, eaque ipsa quae<br> abillo inventore veritatis et magnam </p>
                                        </div>
                                        <div class="learn-more wow slideInUp" data-wow-duration="1.3s" data-wow-delay=".3s">
                                            <a href="about-us.php">Learn more</a>
                                        </div>	
                                    </div>	
                                </div>	
                                <div class="slide-caption-img slideInUp  animated" data-wow-delay="1.2s" data-wow-duration="0.5s">
                                    <img src="img/slider/caption.png" alt="">
                                </div>			
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--slider section end-->
    <!--About us start-->
    <div class="about-us ptb-110">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-img">
                        <img src="img/about/1.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-desc">
                        <h2>about us</h2>
                        <h5>we are a Lorem ipsum dolor sit amet. </h5>
                        <div class="about-content">
                            <p class="text1">
                                Lorem ipsum dolor sit amet, conse ctetur adip isicing elit, sed do eiusmod yth tempor incididunt ut labore et dolore magna aliqua. Ut hyyenim ad minimthy veniam, quis nostrud exercitation ullamco accusanest, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                            </p>
                            <p class="text2">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod mpor incididunt ut labore et dolore magna aliqua. 
                            </p>
                        </div>
                        <a href="about-us.php">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--About us end-->
    <!--Services section start-->
    <div class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="section-title text-center">
                        <h2>our services</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed djhyhdo eiusmod tempor incididunt ut labore et dolore magna aliqua.       </p>
                    </div>
                </div>
            </div>
            <!--Services desc- inner-->
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-1.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>architect</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-2.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>planning</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-5.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>interiors</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-4.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>home design</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-3.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>land analysis</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                    <div class="single-services">
                        <div class="services-head">
                            <div class="services-img">
                                <!-- <img src="img/service/i-6.png" alt=""> -->
                            </div>
                            <div class="services-title">
                                <h5>furniture design</h5>
                            </div>
                        </div>
                        <div class="services-text">
                            <p>Lorem ipsum dolor slgit amet, consectetur adipisicing elit, se dgtgdo eiusmod tempor incididunt ut labore</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="services-section-img">
                        <img src="img/service/1.png" alt="">
                    </div>
                </div>
            </div>
            <!--Services desc- inner end-->
        </div>
    </div>
    <!--Services section end-->
    <!--Our project section start-->
    <div class="our-project pb-95">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="section-title text-center">
                        <h2>our pruject</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed djhyhdo eiusmod tempor incididunt ut labore et dolore magna aliqua.       </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project1.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project2.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project3.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project4.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project5.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-project twon terrace width">
                        <div class="project-img">
                            <img src="img/project/project6.jpg" alt="">
                            <div class="project-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="text-center btn-view">
                <a href="project.php">View More</a>
            </div>
        </div>
    </div>
    <!--Our project section end-->
    
    <!--blog section start -->
    <div class="products-section ptb-110">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="section-title text-center">
                        <h2>our products</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed djhyhdo eiusmod tempor incididunt ut labore et dolore magna aliqua.       </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product1.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product2.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product3.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product4.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product5.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-products">
                        <div class="products-thumbnail">
                            <a href="#">
                                <img src="img/product/product6.jpg" alt="">
                            </a>
                            <div class="products-view">
                                <a href="details.php"><i class="zmdi zmdi-plus-circle"></i></a>
                            </div>
                        </div>
                        <div class="products-action">
                            <div class="products-date">
                                
                            </div>
                            <div class="products-title">
                                <h5><a href="details.php">Product Name</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center btn-view">
                <a href="project.php">View More</a>
            </div>
        </div>
    </div>
    <!--blog section end-->
    
<?php include 'include/footer.php'; ?>