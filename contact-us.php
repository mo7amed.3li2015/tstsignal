<?php include 'include/header.php'; ?>

    <!--Breadcrumbs start -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-inner">
                        <div class="breadcrumbs-title text-center">
                            <h2>Contact us</h2>
                        </div>
                        <div class="breadcrumbs-menu">
                            <ul>
                                <li><a href="#">home</a></li>
                                <li>//  Contact us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--Contact us pages start-->
    <div class="contact-pages ptb-110">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="contact_form">
                        <div class="contact-form-title">
                            <h3>LEAVE A MESSAGE</h3>
                        </div>
                        <div class="contact-form-inner">
                            <form id="contact-form" action="mail.php" method="post">
                                <p class="form-messege"></p>
                                <div class="contact_form-top">
                                    <div class="input-field">
                                        <input name="name" type="text" placeholder="Name">
                                        <span>*</span>
                                    </div>
                                    <div class="input-field">
                                        <input name="subject" type="text" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="contact_form-middle">
                                    <div class="input-field">
                                        <input name="email" type="text" placeholder="Email">
                                        <span>*</span>
                                    </div>
                                    <div class="input-field">
                                        <input name="phone" type="text" placeholder="Phone">
                                    </div>
                                </div>
                                
                                <div class="contact-form-bottom">
                                    <textarea name="message" placeholder="Message"></textarea>
                                    <div class="form-submite">
                                        <button type="submit">Send message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                    <div class="get-informed">
                        <div class="contact-form-title">
                            <h3>GET INFORMED</h3>
                        </div>
                        <div class="single-infromed-list">
                            <div class="single-infromed  fix">
                                <div class="inform-details">
                                    <div class="inform-title">
                                        <h2>sam smith</h2>
                                        <p>manager of erctor</p>
                                    </div>
                                    <div class="inform-info">
                                        <ul>
                                            <li><i class="zmdi zmdi-phone-in-talk"></i>Phone : +061012345678</li>
                                            <li><i class="zmdi zmdi-email"></i>hastech@outlook.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-infromed  fix">
                                <div class="inform-details">
                                    <div class="inform-title">
                                        <h2>anika mollik</h2>
                                        <p>CEo of erctor</p>
                                    </div>
                                    <div class="inform-info">
                                        <ul>
                                            <li><i class="zmdi zmdi-phone-in-talk"></i>Phone : +061012345678</li>
                                            <li><i class="zmdi zmdi-email"></i>hastech@outlook.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="single-infromed  fix">
                                <div class="inform-details">
                                    <div class="inform-title">
                                        <h2>designer of erctor</h2>
                                        <p>manager of erctor</p>
                                    </div>
                                    <div class="inform-info">
                                        <ul>
                                            <li><i class="zmdi zmdi-phone-in-talk"></i>Phone : +061012345678</li>
                                            <li><i class="zmdi zmdi-email"></i>hastech@outlook.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact us pages end-->

<?php include 'include/footer.php'; ?>