
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tstsignal | Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <!-- <link href="img/apple-touch-icon.png" type="images/x-icon" rel="shortcut icon"> -->
    
    <!-- All css files are included here. -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/core.css">
    <link rel="stylesheet" href="css/core.css">
    <link rel="stylesheet" href="css/shortcode/shortcodes.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Modernizr JS -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!--Header section start-->
    <header class="header">
        <div class="header-inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 hidden-md hidden-lg col-xs-12">
                        <div class="logo">
                            <a href="index.php"><img src="img/logo/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-12 hidden-sm hidden-xs">
                        <div class="menu">
                            <nav>
                                <ul>
                                    <li class="logo">
                                        <a href="index.php">
                                            <img src="img/logo/logo.png" alt="">
                                            <!-- LOGO -->
                                        </a>
                                    </li>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About us</a></li>
                                    <li><a href="project.php">Our products</a></li>
                                    <li><a href="project.php">Our Projects</a></li>
                                    <li><a href="contact-us.php">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile menu start -->
            <div class="mobile-menu-area hidden-lg hidden-md">
                <div class="container">
                    <div class="col-md-12">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About us</a></li>
                                <li><a href="project.php">Our Products</a></li>
                                <li><a href="our-team.php">Our Projects</a></li>
                                <li><a href="contact-us.php">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- Mobile menu end -->
            
        </div>
    </header>
    <!--Header section end-->