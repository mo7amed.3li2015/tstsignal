        
    <!--Brand section start-->
    <div class="brand-section ptb-110">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-list">
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/2.jpg" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/4.jpg" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/5.jpg" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img class="img-fluid" src="img/brand/6.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/7.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                        <div class="single-brand">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Brand section end-->
    
    <!--Contact addresses start-->
    <div class="contact-address">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm col-xs-12">
                    <div class="single-contact-address">
                        <div class="single-contact-icon">
                            <i class="zmdi zmdi-pin"></i> 
                        </div>
                        <div class="single-contact-desc">
                            <p>House No 08, Road No 08,</p>
                            <p>Mah Hya, Dhaka, Bangladesh</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-contact-address">
                        <div class="single-contact-icon">
                            <i class="zmdi zmdi-email"></i>
                        </div>
                        <div class="single-contact-desc">
                            <p>Username@gmail.com</p>
                            <p>Damo@gmail.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-contact-address">
                        <div class="single-contact-icon">
                            <i class="zmdi zmdi-phone"></i>
                        </div>
                        <div class="single-contact-desc">
                            <p>+660 256 24857</p>
                            <p>+660 256444 24857</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact addresses end-->
    
    <!--Footer section start-->
    <footer class="footer-area overlay">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-footer about">
                            <div class="single-footer-title">
                                <h5>about us</h5>
                            </div>
                            <div class="single-footer-desc">
                                <div class="single-footer-logo">
                                    <!-- <a href="#"><img src="img/logo/logo-2.png" alt=""></a> -->
                                </div>
                                <div class="single-footer-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicin elit, sed dgtho eiusmod tempor incididunt utffrikfr labore et dolore magna aliqua.</p>
                                </div>
                                <div class="single-footer-social">
                                    <a href="#"><i class="zmdi zmdi-facebook"></i></a>
                                    <a href="#"><i class="zmdi zmdi-instagram"></i></a>
                                    <a href="#"><i class="zmdi zmdi-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-footer navigation">
                            <div class="single-footer-title">
                                <h5>Navigation</h5>
                            </div>
                            <div class="single-footer-desc">
                                <div class="single-footer-nav">
                                    <ul>
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="about-us.php">About us</a></li>
                                        <li><a href="project.php">Our Products</a></li>
                                        <li><a href="our-team.php">Our Projects</a></li>
                                        <li><a href="contact-us.php">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-footer nrewsletter">
                            <div class="single-footer-title">
                                <h5>newsletter singup</h5>
                            </div>
                            <div class="single-footer-desc">
                                <div class="single-footer-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                </div>
                                <div class="single-foote-newsletter">
                                    <div class="newsletter-form">
                                        <form action="#">
                                            <input type="text" placeholder="Email Address...">
                                            <button type="submit">Subscribe</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright text-center">
                            <p>Copyright <i class="fa fa-copyright"></i> 2019 <span>Tstsignal</span> All rights reserved. </p>
                            <p>Programming and Designed by <a href="https://www.clicktopass.com/" target="_blank">Clicktopass</a> Company</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--Footer section end-->

    <!-- All jquery file included here -->
    <script src="js/vendor/jquery-1.12.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/packery-mode.pkgd.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>