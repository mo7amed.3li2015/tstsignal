<?php include 'include/header.php'; ?>
    <!--Breadcrumbs start -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-inner">
                        <div class="breadcrumbs-title text-center">
                            <h2>Project Name</h2>
                        </div>
                        <div class="breadcrumbs-menu">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li>//Project Name</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
     <!--Blog Details pages-->
    <div class="blog-details pt-110">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="articles">
                        <div class="articles-thumbnail">
                            <img src="img/blog/blog-details.jpg" alt="">
                        </div>
                        <div class="post-title">
                            <h3>we have replaced by new design</h3>
                        </div>
                        
                        <div class="post-exerpt">
                            <div class="post-text1">
                                <p class="text1">
                                    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur.  
                                </p>
                                <p class="text2">From a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" </p>
                            </div>
                            <div class="post-text2">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College Latin professor at Hampden-Sydney College </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-12">
                </div>
            </div>
        </div>
    </div>
    <!--Blog Details pages end-->   

<?php include 'include/footer.php'; ?>