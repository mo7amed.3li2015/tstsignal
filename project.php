<?php include 'include/header.php'; ?>

    <!--Breadcrumbs start -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-inner">
                        <div class="breadcrumbs-title text-center">
                            <h2>our Porject</h2>
                        </div>
                        <div class="breadcrumbs-menu">
                            <ul>
                                <li><a href="#">home</a></li>
                                <li>// Our Porject</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--Services section start-->
    <div class="services-pages pt-110">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="section-title text-center">
                        <h2>our projetc</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed djhyhdo eiusmod tempor incididunt ut labore et dolore magna aliqua.       </p>
                    </div>
                </div>
            </div>
            <!--Services desc- inner-->
            <div class="our-project-page-inner">
                <div class="row">
                   <div class="col-md-12">
                        <div class="project-tab-descriptio">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="all">
                                    <div class="project-list-inner">
                                       <div class="row">
                                            <div class="project_list">
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="town">
                                    <div class="project-banner-desc overlay">
                                        <div class="project-banner-inner">
                                            <div class="project-text">
                                                <div class="banner-top-text">
                                                    <h3>Dummy Title Of Projects</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                </div>
                                            </div>
                                            <div class="project_view">
                                                <a href="details.php">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-list-inner">
                                       <div class="row">
                                            <div class="project_list">
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="holiday">
                                    <div class="project-banner-desc overlay">
                                        <div class="project-banner-inner">
                                            <div class="project-text">
                                                <div class="banner-top-text">
                                                    <h3>Dummy Title Of Projects</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                </div>
                                            </div>
                                            <div class="project_view">
                                                <a href="details.php">view more</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-list-inner">
                                       <div class="row">
                                            <div class="project_list">
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                                <div class="single-project-display">
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-1.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-2.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-3.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                    <div class="single_project fix">
                                                        <div class="col-md-6">
                                                            <div class="single_project-img">
                                                                <img src="img/project/p-4.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="single_project-content">
                                                                <div class="project-text">
                                                                    <div class="banner-top-text">
                                                                        <h3>Dummy Title Of Projects</h3>
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dofri eiusmod tempor incididunt ut labore et dolore magn a aliqua. Ut enim ajuthd minim veniam, quis nostrud exercitatiosit amet</p>
                                                                    </div>
                                                                </div>
                                                                <div class="project_view">
                                                                    <a href="details.php">view more</a>
                                                                </div>
                                                            </div>
                                                        </div>       
                                                    </div>
                                                </div>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Services desc- inner end-->
        </div>
    </div>
    <!--Services section end-->
<?php include 'include/footer.php'; ?>