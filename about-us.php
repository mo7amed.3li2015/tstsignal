<?php include 'include/header.php'; ?>
    
    <!--Breadcrumbs start -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-inner">
                        <div class="breadcrumbs-title text-center">
                            <h2>About us</h2>
                        </div>
                        <div class="breadcrumbs-menu">
                            <ul>
                                <li><a href="#">home</a></li>
                                <li>// About us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--About us start-->
    <div class="about-us ptb-110">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-img">
                        <img src="img/about/1.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="about-desc">
                        <h2>Traffic Signal Technology Factory</h2>
                        <h5>we are a studio located in usa </h5>
                        <div class="about-content">
                            <p class="text1">
                                Lorem ipsum dolor sit amet, conse ctetur adip isicing elit, sed do eiusmod yth tempor incididunt ut labore et dolore magna aliqua. Ut hyyenim ad minimthy veniam, quis nostrud exercitation ullamco accusanest, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit
                            </p>
                            <p class="text2">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod mpor incididunt ut labore et dolore magna aliqua. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="about-desc">
                        <p class="text1">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea iusto fugiat vel, exercitationem, at praesentium doloribus quae saepe eligendi deleniti itaque natus minus voluptatem eveniet veniam quibusdam maiores explicabo facere?
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea iusto fugiat vel, exercitationem, at praesentium doloribus quae saepe eligendi deleniti itaque natus minus voluptatem eveniet veniam quibusdam maiores explicabo facere?
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea iusto fugiat vel, exercitationem, at praesentium doloribus quae saepe eligendi deleniti itaque natus minus voluptatem eveniet veniam quibusdam maiores explicabo facere?
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--About us end-->

    <?php include 'include/footer.php'; ?>